#!/bin/bash

############################################################
#
#  Description : déploiement d'une plateforme de test à l'aide
#
#  Auteur : Cristanel
#
#  Date : 01/11/2020
#
###########################################################




#si option --create
#distrib='tanel06/debian-systemd-ssh'
distrib=''
creation='no'
type=''

if [ "$1" == "--create-debian" ];then
	distrib='tanel06/debian-systemd-ssh'
	creation='yes'
	type='debian'
fi

if [ "$1" == "--create-centos" ];then
	distrib='tanel06/centos-systemd-ssh'
	creation='yes'
	type='centos'
fi

if [ "$creation" == "yes" ];then
	
    # définition du nombre de conteneur
    nb_machine=1
    if [ "$2" != "" ];then
        nb_machine=$2
    fi

    # rapatriement de l'image si elle n'exsiste pas
	echo "Installation de l'image "
	docker pull $distrib
  
	# setting min/max
	min=1
	max=0

  # récupération de idmax sur les noms des images dockers
    idmax=`docker ps -a --format "{{ .Names }}" |grep tanel-node | sed s/".*-node-"//g  | sort -nr | head -1`

	# redéfinition de min et max
	min=$(($idmax + 1))
	max=$(($idmax + $nb_machine))

    # création des conteneurs
	echo "Création : ${nb_machine} conteneurs..."

  # lancement des conteneurs
	for i in $(seq $min $max);do
        echo ""
		echo "=========> Conteneur ${type}-tanel-node-${i} ====================>"

		if [ "$type" == "debian" ]; then
        	docker run -tid -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name ${type}-tanel-node-${i} $distrib
		else
			docker run -tid -v /sys/fs/cgroup:/sys/fs/cgroup:ro -e ROOT_PASS="test" --name ${type}-tanel-node-${i} $distrib
		fi

        echo "=========> création de l'utilisateur tanel  ========================>"
		docker exec -ti ${type}-tanel-node-${i} /bin/sh -c "useradd -m tanel ; echo 'tanel:test' | chpasswd"
		docker exec -ti ${type}-tanel-node-${i} /bin/sh -c "mkdir  /home/tanel/.ssh && chmod 700  /home/tanel/.ssh && chown tanel:tanel  /home/tanel/.ssh"
        docker cp $HOME/.ssh/id_rsa.pub ${type}-tanel-node-${i}:/home/tanel/.ssh/authorized_keys
        docker exec -ti ${type}-tanel-node-${i} /bin/sh -c "chmod 600  /home/tanel/.ssh/authorized_keys && chown tanel:tanel  /home/tanel/.ssh/authorized_keys"
		docker exec -ti ${type}-tanel-node-${i} /bin/sh -c "echo 'tanel   ALL=(ALL:ALL) NOPASSWD: ALL'>>/etc/sudoers"
		
		if [ "$type" == "debian" ]; then
        	docker exec -ti ${type}-tanel-node-${i} /bin/sh -c "service ssh start"
		fi
		
		echo "Conteneur ${type}-tanel-node-${i} créé"
	done


# si option --destroy
elif [ "$1" == "--destroy" ];then

	echo "Suppression des conteneurs..."
	sudo docker rm -f $(docker ps -a | grep tanel-node | awk '{print $1}')
	echo "Fin de la suppression"


# si option --start
elif [ "$1" == "--start" ];then
  
  echo ""
  sudo docker start $(docker ps -a | grep tanel-node | awk '{print $1}')
  echo ""

# si option --stop
elif [ "$1" == "--stop" ];then
  
  echo ""
  sudo docker stop $(docker ps -a | grep tanel-node | awk '{print $1}')
  echo ""


# si option --infos
elif [ "$1" == "--infos" ];then
  
  echo ""
  echo "Informations des conteneurs : "
  echo ""
	for conteneur in $(docker ps -a | grep tanel-node | awk '{print $1}');do      
		docker inspect -f '   * {{.Name}} - {{.NetworkSettings.IPAddress }}' $conteneur
	done
	echo ""

elif [ "$1" == "--ansible" ];then
	echo ""
	echo "Configuration des nodes ansibles : "
	
	#obtenir id_min et en faire un controle node après
	id_min=`docker ps -a --format "{{ .Names }}" |grep tanel-node | sed s/".*-node-"//g  | sort -nr | tail -1`

	for conteneur in $(docker ps -a | grep tanel-node | awk '{print $1}');do    
		type_dist=`docker inspect -f '{{.Name}}' $conteneur | cut -c2- | awk '{split($0,a,"-");print a[1]}'` 
		ip_addr=`docker inspect -f '{{.NetworkSettings.IPAddress }}' $conteneur`

		id=`docker inspect -f '{{.Name}}' $conteneur | awk '{split($0,b,"-"); print b[4]}'`
		
		if [ "$id" == "$id_min" ]; then

			if [ "$type_dist" == "debian" ]; then
				echo "Configuration ansible non disponible pour une machine debian"
				#docker exec -ti -u root ${type_dist}-tanel-node-${id} /bin/sh -c "apt-get update "
				
			else
				echo " Configuration du master node : ${type_dist}-tanel-node-${id}"
				docker exec -ti -u tanel ${type_dist}-tanel-node-${id} /bin/sh -c "touch /home/tanel/ansible.cfg /home/tanel/hosts"
				docker exec -ti ${type_dist}-tanel-node-${id} /bin/sh -c "yum install ansible -y"
				docker exec -ti ${type_dist}-tanel-node-${id} /bin/sh -c "yum install -y openssh-clients"

				docker exec -ti -u tanel ${type_dist}-tanel-node-${id} /bin/sh -c "ssh-keygen -f /home/tanel/.ssh/id_rsa"

				for conten in $(docker ps -a | grep tanel-node | awk '{print $1}');do      
					my_ip=`docker inspect -f '{{.NetworkSettings.IPAddress }}' $conten`
					if [ "$my_ip" != "$ip_addr" ];then
						docker exec -ti -u tanel ${type_dist}-tanel-node-${id} /bin/sh -c "echo '$my_ip ansible_user=tanel' >> /home/tanel/hosts"
						docker exec -ti -u tanel ${type_dist}-tanel-node-${id} /bin/sh -c "ssh-copy-id -i /home/tanel/.ssh/id_rsa.pub tanel@$my_ip"
					fi
				done
			fi

		else
			
			if [ "$type_dist" == "debian" ]; then
				echo " Configuration ansible non disponible pour une machine debian"
			else
				echo "Configuration ssh du worker node : ${type_dist}-tanel-node-${id}"
				docker exec -ti ${type_dist}-tanel-node-${id} /bin/sh -c "sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/g' /etc/ssh/sshd_config"
				docker exec -ti ${type_dist}-tanel-node-${id} /bin/sh -c "systemctl enable sshd && systemctl start sshd"
			fi
		fi

		#docker inspect -f '{{.Name}} - {{.NetworkSettings.IPAddress }}' $conteneur
	done
	echo ""

# si aucune option affichage de l'aide
else

echo "
NAME : 
      tanel-deploy-nodes - Deployement d'une pltateforme de test avec un ou plusieurs conteneurs dockeurs

SYNOPSIS : 
        ./tanel-deploy-nodes.sh OPTION 

OPTIONS :
		--create-debian : lancer un conteneur debiant 
                    	  lancer N conteneurs debiant si suivie d'un entier N (--create-debian N)
		
		--create-centos : lancer un conteneur centos
                          lancer N conteneurs centos si suivie d'un entier N (--create-centos N)

		--destroy : supprimer les conteneurs créer par le tanel-deploy-node.sh
	
		--infos : caractéristiques des conteneurs (ip, nom, user...)

		--start : redémarrage des conteneurs

		--stop  : arreter les conteneurs

		

"

fi
