# Script permettant de deployer une plateforme de test à l'aide de conteneurs docker

## Objectif 
L'idée est de pouvoir créer facilement une plateforme de test sans utilisés des VMs. 
On peut créer un parc de machines conteneurisées et le supprimer une fois les tests terminés.

## Intérêts
* Faire des petits tests sans créer des VMs (exécuter un playblook sur un groupe de machines par exemple ou faire des tests d'installation)
* Facile à créer 
* Facile à detruire une fois les tests terminés
* Avoir un réseau facilement operationnel 
* Léger car il n'y a pas de noyau embarqué

## Manuel d'utilisation

```

NAME : 
      tanel-deploy-nodes - Deployement d'une pltateforme de test avec un ou plusieurs conteneurs dockeurs

SYNOPSIS : 
        ./tanel-deploy-nodes.sh OPTION 

OPTIONS :
		--create-debian : lancer un conteneur debiant 
                    	   lancer N conteneurs debiant si suivie d'un entier N (--create-debian N)
		
		--create-centos : lancer un conteneur centos
                          lancer N conteneurs centos si suivie d'un entier N (--create-centos N)

		--destroy : supprimer les conteneurs créer par le tanel-deploy-node.sh
	
		--infos : caractéristiques des conteneurs (ip et nom)

		--start : redémarrage des conteneurs

		--stop  : arreter les conteneurs

		--ansible : Installation et configuration ssh d'ansible sur tout le pool de conteneur.
		            Ansible sera installé sur le conteneur avec l'id minimal.

```
### Examples
* créer une machine debian : ``` ./tanel-deploy-nodes.sh --create-debian ```
* créer 5 machines debian : ``` ./tanel-deploy-nodes.sh --create-debian 5```
* créer une machine centos : ``` ./tanel-deploy-nodes.sh --create-centos ```
* créer 5 machine centos : ``` ./tanel-deploy-nodes.sh --create-centos 5```
* connaitre les noms et les adresses IP des machines : ``` ./tanel-deploy-nodes.sh --infos```
* arreter tous les conteneurs : ``` ./tanel-deploy-nodes.sh --stop```
* redémarrage tous les conteneurs : ``` ./tanel-deploy-nodes.sh --start```
* supprimer tous les conteneurs : ``` ./tanel-deploy-nodes.sh --drop```

* accéder à une machine conteneurisée : ```ssh tanel@<ip du machine>```

*_NOTE_* : user and  root (pour centos) password: ```test```